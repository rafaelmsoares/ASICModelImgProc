#ifndef	_UTILS_H_
#define _UTILS_H_

#include "pgm.h"
#include "ppm.h"


void write_ppm(PPMImage *img, char *str); 
void smooth_ppm(PPMImage *PPM_IN, int threads);
void smooth_pgm(PGMImage *PGM_IN, int threads);
void upsidedown_ppm(PPMImage *PPM_IN, int threads);
void upsidedown_pgm(PGMImage *PGM_IN, int threads);
void mirror_ppm(PPMImage *PPM_IN, int threads);
void mirror_pgm(PGMImage *PGM_IN, int threads);
PGMImage* ppm_pgm(PPMImage *PPM_IN, int threads);
void finalize();
void write_pgm(PGMImage *PGM_OUT, char *str);

#endif
