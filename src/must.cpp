// Source:
// https://courses.cs.washington.edu/courses/cse576/01sp/software/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "must.h"

void	checknull(void *p)
{
	if( p==NULL ) {
		printf("NULL pointer!!!\n");
		exit(-1);
	}
}

void	checknull2(void *p, FILE *fp, char *msg)
{
	if(p==NULL) {
		if(fp==NULL) fp=stdout;
		fprintf(fp, "NULL pointer!!!\n");
		if(msg!=NULL) fprintf(fp, "%s\n", msg);
		exit(-1);
	}
}

void	myerror(int err, FILE *fp, char *msg)
{
	if(fp==NULL) fp=stdout;
	fprintf(fp, "\nFatal error encountered:\n");
	if(msg!=NULL)	fprintf(fp, "Message:\n%s\n", msg);
	else		fprintf(fp, "No error message provided.\n");
	fprintf(fp, "Exiting to system with error code: %d\n", err);
	exit(err);
}

void	**alloc2d(int cols, int rows, int unitsize)
{
	int	i;
	char	**p;
	long	ptr_buf_size, data_buf_size, line_size;

	ptr_buf_size = (long)rows*sizeof(void*);
	data_buf_size = (long)rows*(long)cols*(long)unitsize;
	line_size = (long)cols*unitsize;

#define SINGLE_BLOCK 0
#if SINGLE_BLOCK

	/* make sure that the data buffer starts at a double word */
	if(ptr_buf_size%sizeof(double))
		ptr_buf_size=(ptr_buf_size/sizeof(double)+1)*sizeof(double);

	/* allocate memory for both pointers and data buffer */
	p=(char**)malloc( ptr_buf_size + data_buf_size);
	if(p==NULL) return NULL;
	p[0]=(char*)p + ptr_buf_size;	/* skip the pointer part */
#else
	/* allocate memory for pointers */
	/* p=(char**)malloc(ptr_buf_size); */
	p=(char**)calloc(rows, sizeof(void*));
	if(p==NULL) return NULL;
	/* allocate memory for data buffer */
	/* p[0]=(char*)malloc(data_buf_size); */
	p[0]=(char*)calloc(rows*cols, unitsize);
	if(p[0]==NULL) { free(p); return NULL; }
#endif

	for(i=1;i<rows;i++) p[i]= p[i-1] + line_size;
	return ((void**) p);

}

void	free2d(void **p)
{
	if(p==NULL) return;

#if ! SINGLE_BLOCK
	free(p[0]);
#endif
	free(p);
}

int	readdata(char *name, void *buf, long size)
{
	int	i;
	FILE	*fp;

	fp=fopen(name, "rb");
	if(fp==NULL) return -1;
	i=fread(buf, size, 1, fp);
	fclose(fp);
	if(i!=1) return -1;
	return 0;
}

int	writedata(char *name, void *buf, long size)
{
	int	i, j;
	FILE	*fp;

	fp=fopen(name, "wb");
	if(fp==NULL) return -1;
	i=fwrite(buf, size, 1, fp);
	j=fclose(fp);
	if(i!=1 || j) return -1;
	return 0;
}
