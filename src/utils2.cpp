#ifndef	_UTILS2_H_
#define _UTILS2_H_

#include <stdio.h>	/* for NULL */
#include <stdlib.h>
#include <memory.h>
#include <math.h>
#include <omp.h>

#include "utils2.h"

#include "must.h"
#include "pgm.h"
#include "ppm.h"
#include "type.h"


// ----------- ARTTHM ------------------
// Based on source from:
// https://courses.cs.washington.edu/courses/cse576/01sp/software/
void arithm(uchar **image1, uchar **image2, int width, int height, uchar **image_out, int threads){
    int i,j;

    #pragma omp parallel for num_threads(threads) \
    shared(image_out, image1, image2, width, height) private(i,j)
    for(i=0;i<height;i++){
        for(j=0;j<width;j++) {
            image_out[i][j] = image1[i][j] + image2[i][j];
        }
    }
}
// ------------------------------------------

// ----------- THRESHOLD ------------------
// Based on source from:
// https://courses.cs.washington.edu/courses/cse576/01sp/software/
void thresholdpgm(uchar **image, int width, int height, uchar **image_out, int threads){
    int i,j;
    uchar   newval;

    #pragma omp parallel for num_threads(threads) \
    shared(image, image_out, width, height) private(i,j,newval)
    for(i=0;i<height;i++){
        for(j=0;j<width;j++) {
            newval = ( (image[i][j] >= THRSLD) ? 255 : 0 );
            image_out[i][j] = newval;
        }
    }
}
// ------------------------------------------

// ----------- BMORPHOLOGY ------------------
// Based on source from:
// https://courses.cs.washington.edu/courses/cse576/01sp/software/
int	disk_SE(int width, int height, int cen_x, int cen_y, float R, BSE *se)
{
	int	i, j;
	int	itemp1, itemp2;
	float	ftemp;

	se->width = width;
	se->height = height;
	se->x = cen_x;
	se->y = cen_y;
	se->SE = (char**)alloc2d(se->width, se->height, sizeof(char));
	if(se->SE==NULL) return(-1);
	for(i=0;i<se->height;i++)
		for(j=0;j<se->width;j++) {
			itemp1 = i - se->y;
			itemp2 = j - se->x;
			ftemp = sqrt(itemp1*itemp1 + itemp2*itemp2);
			if( ftemp <= R )
				se->SE[i][j] = 1;
			else
				se->SE[i][j] = 0;
		}
	return 0;
}
void	bdilation(uchar **inimage, int width, int height, uchar fg, uchar bg,
		BSE *se, uchar **outimage, int threads)
{
	int	i, j;
	int	x, y;
	int	xx, yy;
  
    for(i=0;i<height;i++)
        for(j=0;j<width;j++)
          if(inimage[i][j]) inimage[i][j]=1;


	for(i=0;i<height;i++)
		memcpy(outimage[i], inimage[i], width*sizeof(char));

    #pragma omp parallel for num_threads(threads)\
    shared(inimage,outimage,se,fg,height,width) private(x,y,i,j,xx,yy)
	for(y=0;y<height;y++)
	    for(x=0;x<width;x++) {
		if( inimage[y][x] == fg )
			for(i=0;i<se->height;i++)
			    for(j=0;j<se->width;j++)
				if(se->SE[i][j]) {
					yy = y + i - se->y;
					xx = x + j - se->x;
					if(	yy>=0 && yy<height
					     && xx>=0 && xx<width )
						outimage[yy][xx] = fg;
				}
	    }

    for(i=0;i<height;i++)                                                                
        for(j=0;j<width;j++)
            if(outimage[i][j]) outimage[i][j]=255;

	return;
}

void	berosion(uchar **inimage, int width, int height, uchar fg, uchar bg,
		BSE *se, uchar **outimage, int threads)
{
	int	i, j;
	int	x, y;
	int	delete_;

    printf("erosion\n");

    for(i=0;i<height;i++)
        for(j=0;j<width;j++)
          if(inimage[i][j]) inimage[i][j]=1;


	for(i=0;i<height;i++)
	memcpy((void*)outimage[i], (const void*)inimage[i], width*sizeof(char));

    #pragma omp parallel for num_threads(threads)\
    shared(inimage,outimage,se,fg,height,width) private(x,y,i,j,delete_)
	for(y=0;y<height;y++){
	    for(x=0;x<width;x++) {
		if( inimage[y][x] != fg ) continue;
		delete_ = 0;
		for(i=0;i<se->height && !delete_;i++)
		    for(j=0;j<se->width;j++)
			if(se->SE[i][j]) {
				int yy = y + i - se->y;
				int xx = x + j - se->x;
				if(	yy<0 || yy>=height || xx<0 || xx>=width ) {
					delete_ = 1;
					break;
				}
				if(inimage[yy][xx] != fg ) {
					delete_ = 1;
					break;
				}
			}
		if( delete_ ) outimage[y][x]=bg;
	    }
    }

    for(i=0;i<height;i++)                                                                
        for(j=0;j<width;j++)
            if(outimage[i][j]) outimage[i][j]=255;

	return;
}
// ------------------------------------------

#endif
