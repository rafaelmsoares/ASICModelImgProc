// Source:
// https://courses.cs.washington.edu/courses/cse576/01sp/software/

#ifndef	_PGM_H_
#define _PGM_H_

int	ReadPGM(char *name, int *width, int *height, unsigned char **(*data) );
int	WritePGM(char *name, int width, int height, unsigned char **data);

#endif
