#ifndef	_UTILS2_H_
#define _UTILS2_H_

void arithm(uchar **image1, uchar **image2, int width, int height, uchar **image_out, int threads);
void thresholdpgm(uchar **image, int width, int height, uchar **image_out, int threads);
int	disk_SE(int width, int height, int cen_x, int cen_y, float R, BSE *se);
void	bdilation(uchar **inimage, int width, int height, uchar fg, uchar bg,
		BSE *se, uchar **outimage, int threads);
void	berosion(uchar **inimage, int width, int height, uchar fg, uchar bg,
		BSE *se, uchar **outimage, int threads);

#endif
