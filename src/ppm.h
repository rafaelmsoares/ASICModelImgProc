#ifndef	_PPM_H_
#define _PPM_H_

#include "type.h"

PPMImage *readPPM(const char *filename);
void writePPM(PPMImage *img, char *name);

#endif
