#include <omp.h>
#include <stdio.h>	/* for NULL */
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include "utils.h"
#include "type.h"
#include "must.h"
#include "pgm.h"
#include "ppm.h"


void write_ppm(PPMImage *img, char *str) {
    FILE *fp;
    fp = fopen(str, "wb");
    if (!fp) {
        fprintf(stderr, "Unable to open file imagem2.ppm\n");
        exit(1);
    }

    fprintf(fp, "P6\n");
    fprintf(fp, "# %s\n", COMMENT);
    fprintf(fp, "%d %d\n", img->x, img->y);
    fprintf(fp, "%d\n", RGB_COMPONENT_COLOR);
    fwrite(img->data, 3 * img->x, img->y, fp);
    fclose(fp);
}


void smooth_ppm(PPMImage *PPM_IN, int threads){
  PPMPixel *data = (PPMPixel*) malloc(sizeof(PPMPixel)*PPM_IN->x*PPM_IN->y);
  int i, j, y, x;
  int total_red, total_blue, total_green;
  for (i = 0; i < PPM_IN->y; i++) {
    for (j = 0; j < PPM_IN->x; j++) {
      total_red = total_blue = total_green = 0;
      for (y = i - ((MASK_WIDTH-1)/2); y <= (i + ((MASK_WIDTH-1)/2)); y++) {
        for (x = j - ((MASK_WIDTH-1)/2); x <= (j + ((MASK_WIDTH-1)/2)); x++) {
          if (x >= 0 && y >= 0 && y < PPM_IN->y && x < PPM_IN->x) {
            total_red += PPM_IN->data[(y * PPM_IN->x) + x].red;
            total_blue += PPM_IN->data[(y * PPM_IN->x) + x].blue;
            total_green += PPM_IN->data[(y * PPM_IN->x) + x].green;
          } //if
        } //for z
      } //for y
      data[(i * PPM_IN->x) + j].red = total_red / (MASK_WIDTH*MASK_WIDTH);
      data[(i * PPM_IN->x) + j].blue = total_blue / (MASK_WIDTH*MASK_WIDTH);
      data[(i * PPM_IN->x) + j].green = total_green / (MASK_WIDTH*MASK_WIDTH);
    }
  }
  for (i = 0; i < PPM_IN->y; i++) {
    for (j = 0; j < PPM_IN->x; j++) {
      PPM_IN->data[(i * PPM_IN->x) + j] = data[(i * PPM_IN->x) + j];
    }
  }
}



void smooth_pgm(PGMImage *PGM_IN, int threads){
  int i, j, y, x;
  unsigned char **data = (unsigned char**)alloc2d(PGM_IN->x, PGM_IN->y, sizeof(uchar));

  int total_gray;
  for (i = 0; i < PGM_IN->y; i++) {
    for (j = 0; j < PGM_IN->x; j++) {
      total_gray = 0;
      for (y = i - ((MASK_WIDTH-1)/2); y <= (i + ((MASK_WIDTH-1)/2)); y++) {
        for (x = j - ((MASK_WIDTH-1)/2); x <= (j + ((MASK_WIDTH-1)/2)); x++) {
          if (x >= 0 && y >= 0 && y < PGM_IN->y && x < PGM_IN->x) {
            total_gray += PGM_IN->data[y][x];
          } //if
        } //for z
      } //for y
      data[i][j] = total_gray / (MASK_WIDTH*MASK_WIDTH);
    }
  }
  for (i = 0; i < PGM_IN->y; i++) {
    for (j = 0; j < PGM_IN->x; j++) {
      PGM_IN->data[i][j] = data[i][j];
    }
  }
}


void upsidedown_ppm(PPMImage *PPM_IN, int threads){
  int i, j, y, x;
  x = PPM_IN->x;
  y = PPM_IN->y;
  PPMPixel *data = (PPMPixel*) malloc(sizeof(PPMPixel)*PPM_IN->x*PPM_IN->y);
  for (i = 0; i < y; i++) {
    for (j = 0; j < x; j++) {
      data[(i * x) + j].red = PPM_IN->data[((y-1 - i) * x) + j].red;
      data[(i * x) + j].green = PPM_IN->data[((y-1 - i) * x) + j].green;
      data[(i * x) + j].blue = PPM_IN->data[((y-1 - i) * x) + j].blue;
    }
  }
  for (i = 0; i < PPM_IN->y; i++) {
    for (j = 0; j < PPM_IN->x; j++) {
      PPM_IN->data[(i * PPM_IN->x) + j] = data[(i * PPM_IN->x) + j];
    }
  }

}

void upsidedown_pgm(PGMImage *PGM_IN, int threads){
  int i, j, y, x;
  x = PGM_IN->x;
  y = PGM_IN->y;

  unsigned char **data = (uchar**)alloc2d(x, y, sizeof(uchar));


  for (i = 0; i < y; i++) {
    for (j = 0; j < x; j++) {
      data[i][j] = PGM_IN->data[y-1-i][j];
    }
  }
  for (i = 0; i < PGM_IN->y; i++) {
    for (j = 0; j < PGM_IN->x; j++) {
      PGM_IN->data[i][j] = data[i][j];
    }
  }

}

void mirror_ppm(PPMImage *PPM_IN, int threads){
  int i, j, y, x;
  x = PPM_IN->x;
  y = PPM_IN->y;
  PPMPixel *data = (PPMPixel*) malloc(sizeof(PPMPixel)*PPM_IN->x*PPM_IN->y);
  for (i = 0; i < y; i++) {
    for (j = 0; j < x; j++) {
      data[(i * x) + j].red = PPM_IN->data[(i * x) + x - j].red;
      data[(i * x) + j].green = PPM_IN->data[(i * x) + x - j].green;
      data[(i * x) + j].blue = PPM_IN->data[(i * x) + x - j].blue;
    }
  }
  for (i = 0; i < PPM_IN->y; i++) {
    for (j = 0; j < PPM_IN->x; j++) {
      PPM_IN->data[(i * PPM_IN->x) + j] = data[(i * PPM_IN->x) + j];
    }
  }

}


void mirror_pgm(PGMImage *PGM_IN, int threads){
  int i, j, y, x;
  x = PGM_IN->x;
  y = PGM_IN->y;

  unsigned char **data = (uchar**)alloc2d(x, y, sizeof(uchar));
 

  for (i = 0; i < y; i++) {
    for (j = 0; j < x; j++) {
      data[i][j] = PGM_IN->data[i][x-1-j];
    }
  }


  for (i = 0; i < PGM_IN->y; i++) {
    for (j = 0; j < PGM_IN->x; j++) {
      PGM_IN->data[i][j] = data[i][j];
    }
  }

}


PGMImage* ppm_pgm(PPMImage *PPM_IN, int threads){
  int i,j,x,y;
  x = PPM_IN->x;
  y = PPM_IN->y;
  PGMImage *img = (PGMImage*) malloc(sizeof(PGMImage));
  img->x = x;
  img->y = y;

  img->data = (uchar**)alloc2d(x, y, sizeof(uchar));

  int total_red, total_blue, total_green, gray_scale;
  
  for (i = 0; i < y; i++) {
    for (j = 0; j < x; j++) {
      total_red = PPM_IN->data[(i * PPM_IN->x) + j].red;
      total_blue = PPM_IN->data[(i * PPM_IN->x) + j].blue;
      total_green = PPM_IN->data[(i * PPM_IN->x) + j].green;								
      gray_scale  =  0.2126 * total_red   +   0.7152 * total_blue   +   0.0722 * total_green;
      img->data[i][j] = gray_scale;
    }
  }
  return img;
}

void finalize(){
//    writePPM(image);

    //fprintf(stdout, "\n%0.6lfs\n", t_end - t_start);  
  //  free(image_copy);
   // free(image);
 
}

void write_pgm(PGMImage *PGM_OUT, char *str){
	WritePGM(str, PGM_OUT->x,PGM_OUT->y, PGM_OUT->data);
	return;
}
