#ifndef	_TYPE_H_
#define _TYPE_H_


#define SEsize 11 // morph
#define THRSLD 100 // threshold
#define MASK_WIDTH 5 // smooth

#define COMMENT "Histogram_GPU"
#define RGB_COMPONENT_COLOR 255

#define PGM_RAW		"P5"
#define PPM_RAW		"P6"

typedef struct {
    unsigned char red, green, blue;
} PPMPixel;

typedef struct {
    int x, y;
    PPMPixel *data;
} PPMImage;

// morphology
typedef	struct {
	short	width, height;	/* size of the structuring element */
	short	x, y;		/* origin of the structuring element */
	char	**SE;
} BSE;	/* Binary Structuring Element */


#define MASK_WIDTH 5


typedef struct {
    int x, y;
    unsigned char **data;
} PGMImage;

typedef unsigned char	uchar;


#endif
