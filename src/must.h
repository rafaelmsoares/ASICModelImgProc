// Source:
// https://courses.cs.washington.edu/courses/cse576/01sp/software/

#ifndef __must_h__
#define __must_h__
#include <stdio.h>

#ifndef	_uchar_defined_
#define	_uchar_defined_
typedef unsigned char	uchar;
/*
typedef unsigned short	ushort;
typedef unsigned int	uint;
typedef unsigned long	ulong;
*/
#endif

#ifndef	_point_defined_
#define	_point_defined_
typedef struct {
	short	x, y;
} point;
#endif

void	checknull(void *p);
void	checknull2(void *p, FILE *fp, char *msg);
void	myerror(int err, FILE *fp, char *msg);

void	**alloc2d(int cols, int rows, int unitsize);
void	free2d(void **p);

int	readdata(char *name, void *buf, long size);
int	writedata(char *name, void *buf, long size);

#endif

