// Source:
// https://courses.cs.washington.edu/courses/cse576/01sp/software/

#include <stdio.h>
#include <string.h>
#include "pgm.h"

typedef	unsigned char	uchar;
void	**alloc2d(int cols, int rows, int unitsize);
void	free2d(void **p);

int	ReadPGM(char *name, int *width, int *height, uchar **(*data) )
{
	int	i;
	uchar	**dp;
	char	cbuf[256];
	FILE	*fp;

	*data=NULL;
	fp=fopen(name, "rb");
	if(fp==NULL) {
		return -1;
	}

	fgets(cbuf, 256, fp);	/* magic number, P5 for PGM file */
	if(strcmp(cbuf,"P5\n")) {
        printf("=====\n");
        fputs(cbuf, stdout);
        printf("=====\n");
		fclose(fp);
		return -2;
	}

	for(;;) {
		fgets(cbuf, 256, fp);
		if(cbuf[0]!='#') break; /* comments */
	}
	i=sscanf(cbuf, "%d %d", width, height);
	if(i!=2) {
		fclose(fp);
		return -3;
	}
	fgets(cbuf, 256, fp);	/* 255 */

	dp=(uchar **)alloc2d((*width), (*height), sizeof(uchar));
	if(dp==NULL) {
		fclose(fp);
		return -4;
	}

	i=fread(dp[0], (*width), (*height), fp);
	fclose(fp);
	if(i!=(*height)) {
		free2d((void**)dp);
		return -5;
	}

	(*data)=dp;
	return 0;
}

int	WritePGM(char *name, int width, int height, uchar **data)
{
	int	i, j;
	FILE	*fp;

	fp=fopen(name, "wb");
	if(fp==NULL) return (-1);
	fprintf(fp, "P5\n");	/* magic number for PGM files */
	/* maybe should put some comment lines here */
	fprintf(fp, "%d %d\n", width, height);
	fprintf(fp, "255\n");	/* 255 */

	i=fwrite(data[0], width, height, fp);
	j=fclose(fp);
	if(i!=height || j) return (-1);

	return 0;
}
