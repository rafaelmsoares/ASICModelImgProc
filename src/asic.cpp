
SC_MODULE (asic_module) {

    sc_in<bool>		clk;
    sc_in<int>		reset;


void finalize();

enum States{rst, execute, done};
enum Configurations{ppm_pgm_conf, mirror_pgm_conf, mirror_ppm_conf, smooth_pgm_conf,
									 smooth_ppm_conf, upsidedown_pgm_conf, upsidedown_ppm_conf,
									 bdilation_conf, berosion_conf, thresholdpgm_conf, arithm_conf};

States state;
Configurations set;

// Memory
PPMImage *PPM_IN;
PPMImage *PPM_OUT;
PGMImage *PGM_IN;
PGMImage *PGM_OUT;
BSE *se;
char* outname;
int threads;

char op;



void main_process(){
	if(reset.read()){
		state = rst;
	} else if(clk.posedge()) {
		switch(state){
			case rst: 
				state = execute; 
			break;

			case execute: 
                switch(op){
                    case 'a':	PGM_IN = ppm_pgm(PPM_IN, threads);
                        break;
                            
                    case 'b':	mirror_pgm(PGM_IN, threads); 			
                        break;
                            
                    case 'c':	mirror_ppm(PPM_IN, threads); 
                        break;
                    
                    case 'd':	smooth_pgm(PGM_IN, threads);
                        break;
                    
                    case 'e':	smooth_ppm(PPM_IN, threads);
                        break;
                    
                    case 'f':	upsidedown_pgm(PGM_IN, threads);
                        break;
                    
                    case 'g':	upsidedown_ppm(PPM_IN, threads);
                        break;
                    
                    case 'h':	bdilation(PGM_IN->data, PGM_IN->x, PGM_IN->y, 1, 0, se, PGM_OUT->data, threads); 
                        break;
            
                    case 'i':	berosion(PGM_IN->data, PGM_IN->x, PGM_IN->y, 1, 0, se, PGM_OUT->data, threads);
                        break;

                    case 'j':	thresholdpgm(PGM_IN->data, PGM_IN->x, PGM_IN->y, PGM_OUT->data, threads);
                        break;
                      
                    case 'k':	arithm(PGM_IN->data, PGM_IN->data, PGM_IN->x, PGM_IN->y, PGM_OUT->data, threads);
                        break;
                    
                    default: break;
                }
            state = done;

           case done:
             sc_stop();
             break;		
           default: break;		
    }
  }
}


  SC_CTOR (asic_module) {

    SC_METHOD(main_process);
    sensitive << clk << reset;

    PGM_IN = (PGMImage*)malloc(sizeof(PGMImage));
		PGM_OUT = (PGMImage*)malloc(sizeof(PGMImage));  
  }
};
