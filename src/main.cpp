// All systemc modules should include systemc.h header file
#include <systemc.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#include "type.h"
#include "must.h"
#include "pgm.h"
#include "ppm.h"
#include "utils.h"
#include "utils2.h"
#include "asic.cpp"

void execute(int width, int height, BSE *se, uchar** outimagePGM, uchar** inimagePGM, 
            PPMImage *outimagePPM, PPMImage* inimagePPM, char op, char *outname, int threads)
{
    double start, end, duracao;

	sc_report_handler::set_actions (SC_WARNING, SC_DO_NOTHING);

    sc_signal<int> reset;
    asic_module asic("asic");
	
	asic.PPM_IN = inimagePPM;
	asic.PPM_IN	= outimagePPM;
	
	
	asic.PGM_IN->x = width;
	asic.PGM_IN->y = height;
	asic.PGM_IN->data = inimagePGM;
	
	
	asic.PGM_OUT->x = width;
	asic.PGM_OUT->y = height;
	asic.PGM_OUT->data = outimagePGM;
	asic.outname = outname;
	asic.se=se;
	asic.op=op;
	asic.threads = threads;
	
	start = omp_get_wtime();

    sc_clock clk ("my_clock",100,5);
    asic.clk(clk);
    asic.reset(reset);

    reset.write(true);

    sc_start(300, SC_NS);

    reset.write(false);
    sc_start(1600000, SC_NS);

    end = omp_get_wtime();
    duracao = end - start;
    printf("%lf\n",duracao);

    switch(op){
        case 'a':
        case 'b':
        case 'd':
        case 'f':
            write_pgm(asic.PGM_IN, outname);
            break;
        case 'h':
        case 'i':
        case 'j':
        case 'k':
            write_pgm(asic.PGM_OUT, outname);
            break;
        default:
            write_ppm(asic.PPM_IN, outname);
    }

    return;
}



int sc_main(int argc, char* argv[]){

    sc_core::sc_report_handler::set_actions( "/IEEE_Std_1666/deprecated",
                                           sc_core::SC_DO_NOTHING );

    uchar	**inimagePGM, **outimagePGM;
    PPMImage *inimagePPM, *outimagePPM;
    char op;
    int	width, height;
    char	*inname, *outname;
    char     cbuf[3];
    int threads;

    inimagePPM = outimagePPM = NULL;
    inimagePGM = outimagePGM = NULL;

    // USAGE: imgProc <a,b,c,d,e,f,g,h> <n_thread> <inp_imp> <out_img>  
    sscanf(argv[1], "%c", &op);
    sscanf(argv[2], "%d", &threads);
    inname=argv[3];
    outname=argv[4];

    if(	op!='d' && op!='e' && op!='a' && op!='t' && op!='s'){
        printf("Invalid input parameter(s)\n");
    }

    /* open the input image file */
	FILE *f = fopen( inname, "rb" );
    if( f == NULL ) {
        fprintf( stderr, "Error opening %s\n", inname );
        exit( -1 );
    }

	/* read the magic number in the first line */
	fscanf( f, "%s\n", cbuf );
	if( strcmp( cbuf, PPM_RAW ) == 0 ) {
		/* we have a PPM image */
		inimagePPM = readPPM(inname);
		outimagePPM = readPPM(inname);
	} else if( strcmp( cbuf, PGM_RAW ) != 0 ) {
	/* image has an unknown format */
		fprintf( stderr, "Input image should be in either PGM or PPM format\n" );
		fclose( f );
		exit( -1 );
	} else {
		/* we have a PGM image */
		ReadPGM(inname, &width, &height, &inimagePGM);
		outimagePGM=(uchar**)alloc2d(width, height, sizeof(uchar));
	}

	// constants used by erosion & dilation
	int	xorig, yorig;
	BSE	se;
	xorig=yorig=SEsize/2;
	disk_SE(SEsize, SEsize, xorig, yorig, SEsize/2, &se);
	
	execute(width,height,&se,outimagePGM,inimagePGM,outimagePPM,inimagePPM,op,outname,threads);

	return 0;

}

