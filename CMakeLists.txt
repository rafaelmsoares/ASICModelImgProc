cmake_minimum_required(VERSION 2.8)

project(ddlms)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR})

#------------------------------------------------------------------------------
# Package: SystemC
#------------------------------------------------------------------------------
set(USE_SYSTEM_SYSTEMC OFF CACHE BOOL "Use system SystemC")

include(External_SystemC)

include_directories( ${SYSTEMC_INCLUDES} )

#------------------------------------------------------------------------------
# Package: OpenMP
#------------------------------------------------------------------------------
find_package(OpenMP)

if (OPENMP_FOUND)
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
else()
    message( FATAL_ERROR "OpenMP library not found." )
endif()

#------------------------------------------------------------------------------
# Flags
#------------------------------------------------------------------------------
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -Wall -g -D_SIM_DDLMS_")

#------------------------------------------------------------------------------
# Include
#------------------------------------------------------------------------------
include_directories( ${CMAKE_CURRENT_BINARY_DIR} )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )

set(SRC_PATH "src/")

include_directories(${SRC_PATH})

set(SOURCES_COMMON ${SRC_PATH}/main.cpp
                   ${SRC_PATH}/must.cpp
                   ${SRC_PATH}/pgm.cpp
                   ${SRC_PATH}/ppm.cpp
)

set(SOURCES_PARALLEL ${SOURCES_COMMON}
                     ${SRC_PATH}/utils.cpp
                     ${SRC_PATH}/utils2.cpp
)

set(SOURCES_SERIAL ${SOURCES_COMMON}
                   ${SRC_PATH}/utils-serial.cpp
                   ${SRC_PATH}/utils2-serial.cpp
)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
file(COPY input DESTINATION ${CMAKE_BINARY_DIR})
file(COPY scripts DESTINATION ${CMAKE_BINARY_DIR})

add_executable(imgProc_ser ${SOURCES_SERIAL})
add_dependencies(imgProc_ser SYSTEMC)
target_link_libraries(imgProc_ser ${SYSTEMC_LIBRARIES})

add_executable(imgProc ${SOURCES_PARALLEL})
add_dependencies(imgProc SYSTEMC)
target_link_libraries(imgProc ${SYSTEMC_LIBRARIES})
#------------------------------------------------------------------------------
# Tests
# ------------------------------------------------------------------------------

set(CREATE_RESULT_DIR rm -rf results && mkdir results && mkdir results/serial results/parallel)
set(POS_RUN_SPEEDUP scripts/speedup.sh)

add_custom_target(run COMMAND ${CREATE_RESULT_DIR} && ${POS_RUN_SPEEDUP} )
