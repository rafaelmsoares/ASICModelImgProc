library(tikzDevice)

files <- list.files(path="../results/", pattern="*.csv_split", full.names=T, recursive=FALSE) 

plotM<-function(x){
    bench <- read.table(file=x, header=TRUE, sep="", row.names = 1)
    sg <- barplot(t(as.matrix(bench)), beside=TRUE, xaxt="n", ylab="speedup", main=substring(strsplit(basename(x), "\\.csv_split")[[1]], 1) )
text(sg, y=t(as.matrix(bench)), label=round(t(as.matrix(bench)), 1), cex = .75, srt= 90, adj = c(-0.3, 0.5))
    text(x=colMeans(sg)-.25, par("usr")[3], labels = rownames(bench), srt = 45, adj = c(1.1,1.1), xpd = TRUE, cex=.9)
}


lapply(files, function(x) {
    plotM(x)
})

