#!/bin/bash

actual_path=$(readlink -f "${BASH_SOURCE[0]}")
APP_ROOT=$(dirname $(dirname "$actual_path"))

export SYSTEMC_DISABLE_COPYRIGHT_MESSAGE=1

declare -A filter_ppm=( ["a"]="ppm_to_pgm" ["c"]="mirror_ppm" ["e"]="smooth_ppm" ["g"]="upsidedown_ppm" )
declare -A filter_pgm=(["h"]="dilation_pgm" ["i"]="erosion_pgm" ["j"]="threshold_pgm" ["k"]="image_sum_pgm")

declare -a n_thread=(2 4 8 16)
csv=false


for image_type in 'pgm' 'ppm'; do 
    for file in "${APP_ROOT}"/input/"${image_type}"/*; do
        filename=$(basename "$file")
        printf "$filename\n"
            
        if [ $csv = true ]; then
            printf "filters "
            for num_thread in "${n_thread[@]}"; do
                printf "$num_thread "
            done
            printf "\n"
        else 
            printf " -----------------------------------------------------\n"
            printf "\t\t"
            for num_thread in "${n_thread[@]}"; do
                printf "$num_thread\t"
            done
            printf "\n-----------------------------------------------------\n"
        fi

        if [ $image_type = 'ppm' ]; then
            assoc_array_string=$(declare -p filter_ppm)
            eval "declare -A filter="${assoc_array_string#*=}
        else
            assoc_array_string=$(declare -p filter_pgm)
            eval "declare -A filter="${assoc_array_string#*=}
        fi

        for filter_alg in "${!filter[@]}"; do
            printf "%-14s" ${filter[${filter_alg}]}
            # SERIAL
            tempo_serial=$( ${APP_ROOT}/bin/imgProc $filter_alg 1 $file ${APP_ROOT}/results/serial/${filename}_${filter_alg}_serial | tail -n 1)

            for num_thread in 2 4 8 16; do
                # PARALELO
                acc=0
                for run in {1..5}; do
                    tempo=$( ${APP_ROOT}/bin/imgProc $filter_alg $num_thread $file ${APP_ROOT}/results/parallel/${filename}_${filter_alg}_${num_thread} | tail -n 1)
                    acc=$(echo " $acc + $tempo" | bc -l | tr '\n' ' ')
                done

                avr_tempo=$(echo "$acc / 5" | bc -l | tr '\n' ' ')
                speedup=$(echo "scale = 4; $tempo_serial / $avr_tempo" | bc -l | tr '\n' ' ')
                printf  "$speedup\t"
            done
            printf "\n"
        done
        
        if [ $csv = false ]; then
           printf " -----------------------------------------------------\n\n"
        fi

    done
done
