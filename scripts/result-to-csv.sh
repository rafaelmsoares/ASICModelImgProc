
split -l 6 $1 --additional-suffix=.csv_split

for file in *.csv_split
do  
    nome=$(head -1 $file)
    tail -n +2 "$file" > "${nome}.csv_split"
    rm $file
    #mv "$file" "$(head -1 $file).csv"
done
