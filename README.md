Modelo de um sistema ASIC para processamento de imagens utilizando SystemC
=============================



Prerequisites
==============================================
- CMake 2.8
- SystemC 2.3.1


Building with CMake
==============================================
0. Clone the demo repository.

        git clone https://gitlab.com/rafaelmsoares/ASICModelImgProc

1. Change into the repository

        cd ASICModelImgProc

2. Create a new directory for building.

        mkdir build

3. Change into the new directory.

        cd build

4. Run CMake with the path to the project source.

        cmake ..

5. Run make inside the build directory (it may take some time to download and install SystemC):

        make

Running
==============================================

Run all serial and parallel tests:

    make run

Developers
==============================================
- Rafael Mendonça Soares
- Ramon Nepomuceno
